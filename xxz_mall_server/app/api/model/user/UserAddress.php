<?php

namespace app\api\model\user;

use app\common\model\user\UserAddress as UserAddressModel;

/**
 * 用户收货地址模型
 */
class UserAddress extends UserAddressModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
        'app_id',
        'create_time',
        'update_time'
    ];

}